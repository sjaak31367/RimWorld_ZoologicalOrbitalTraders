﻿using System;
using UnityEngine;
using Verse;
using RimWorld;

namespace ZoologicalOrbitalTraders {
    public class ZoologicalOrbitalTraders : Mod {
        public ZoologicalOrbitalTraders(ModContentPack content) : base(content) {
            Settings = GetSettings<ZoologicalOrbitalTradersSettings>();
            if (Settings.zoologicalOrbitalTrader_commonality.Equals(Single.NaN)) {  // If config not yet generated, don't set a commonality of 0... Set it to 1 (or 10)
                Settings.zoologicalOrbitalTrader_commonality = ((ModLister.HasActiveModWithName("Let's Trade!") || ModLister.HasActiveModWithName("Let's Trade! [Continued]")) ? 10 : 1);
            }
            LongEventHandler.ExecuteWhenFinished(ChangeCommonalityOfTraderKindDef_ZoologicalOrbitalTrader);  // Load Orbital_ZoologicalTrader from TraderKinds_Orbital_Zoologic.xml, and change it's commonality to that in the mod-config (but do it a slight bit later, so that the settings are actually propperly loaded).
        }

        public static ZoologicalOrbitalTradersSettings Settings { get; private set; }

        public override void DoSettingsWindowContents(Rect inRect) {
            base.DoSettingsWindowContents(inRect);
            GetSettings<ZoologicalOrbitalTradersSettings>().DoWindowContents(inRect);
        }

        public override string SettingsCategory() {
            return "Zoological Orbital Traders";
        }

        public void ChangeCommonalityOfTraderKindDef_ZoologicalOrbitalTrader() {
            TraderKindDef defOrbital_ZoologicalTrader = DefDatabase<TraderKindDef>.GetNamed("Orbital_ZoologicalTrader");  // Used .GetNamed(string s), not .AllDefs.Named(string s), as that 2nd one does not seem to actually do anything? (or at least not in this context.)
            defOrbital_ZoologicalTrader.commonality = Settings.zoologicalOrbitalTrader_commonality;
        }
    }
}

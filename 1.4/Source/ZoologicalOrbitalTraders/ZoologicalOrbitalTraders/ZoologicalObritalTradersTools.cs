﻿using System;
using System.Linq;
using System.Collections.Generic;
using Verse;
using RimWorld;

namespace ZoologicalOrbitalTraders {
    public static class ZoologicalObritalTradersTools {  // TODO on ZoologicalTraderCaravans-side if mod.exists(ZoologicalOrbitalTraders) then disable Tools on their side.

        // NOTE: This is already in vanilla, dunce me!
        /*[DebugAction("Incidents", "Spawn specific orbital trader", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void SpawnSpecificOrbitalTraderTool() {
            List<DebugMenuOption> list = new List<DebugMenuOption>();
            foreach (TraderKindDef localDef2 in from def in DefDatabase<TraderKindDef>.AllDefs
                                                where def.orbital == true
                                                select def into d
                                                orderby d.defName
                                                select d) {
                TraderKindDef localDef = localDef2;
                list.Add(new DebugMenuOption(localDef.label, DebugMenuOptionMode.Action, delegate() {
                    // TODO add nameGenerator and messageGenerator.
                    SpawnSpecificOrbitalTrader(localDef, "TraderName", Find.CurrentMap, "MessageTitle", "MessageBody");
                }));
            }
            Find.WindowStack.Add(new Dialog_DebugOptionListLister(list));
        }*/

        public static void SpawnSpecificOrbitalTrader(TradeShip tradeShip, Map map, string letterTitle, string letterBody) {
            if (tradeShip != null) {
                if (map != null) {
                    if (map.passingShipManager.passingShips.Count > 4) {
                        map.passingShipManager.RemoveShip(map.passingShipManager.passingShips[0]);
                    }
                    //IncidentParms incident = new IncidentParms { target = Find.CurrentMap };
                    //incident.traderKind = DefDatabase<TraderKindDef>.GetNamed("Orbital_ZoologicalTrader");
                    //IncidentDefOf.OrbitalTraderArrival.Worker.TryExecute(incident);  // This isn't used as OrbitalTraderArrival doesn't even check parms.traderKind, and just generates a random one! >:C

                    // Since IncidentWorker_TraderCaravanArrival ignores the passed traderKind, we'll have to do this all manually, as making a new kind of Incident would be kinda silly for a very rare and semi-one-time use. (He said before later making a class containing Tools.)
                    map.passingShipManager.AddShip(tradeShip);
                    tradeShip.GenerateThings();  // Stock it up after putting it on the map.

                    LetterStack letterStack = Find.LetterStack;
                    letterStack.ReceiveLetter(letterTitle, letterBody, LetterDefOf.PositiveEvent);
                } else {
                    Log.Error("You can't spawn a ship outside of a game/map!");
                }
            } else {
                throw new ArgumentNullException("tradeShip");
            }
        }

        public static void SpawnSpecificOrbitalTrader(TraderKindDef traderKind, string traderName, Map map, string letterTitle, string letterBody) {
            if (traderKind != null) {
                TradeShip tradeShip = new TradeShip(traderKind) { name = traderName };
                SpawnSpecificOrbitalTrader(tradeShip, map, letterTitle, letterBody);
            } else {
                throw new ArgumentNullException("traderKind");
            }
        }

        [DebugAction("Spawning", "Spawn all animals", actionType = DebugActionType.ToolMap, allowedGameStates=AllowedGameStates.PlayingOnMap)]
        public static void SpawnAllAnimals() {
            SpawnAllAnimals(Find.CurrentMap, UI.MouseCell());
        }

        public static void SpawnAllAnimals(Map map, IntVec3 loc) {  // WARNING: I DON'T RETURN ANYTHING, NO ERRORS, NO SUCCESSVALUE, NO DATA!
            if (map != null) {
                int totalSpawnedThings = 0;
                List<Pawn> spawnedThings = new List<Pawn>();
                List<PawnKindDef> thingsNotSpawned = new List<PawnKindDef>();
                foreach (PawnKindDef pawnKind in DefDatabase<PawnKindDef>.AllDefs) {
                    try {
                        if (pawnKind.race != ThingDefOf.Human) {  // No humans, as humans have factions and hostility and stuff... And also aren't the intended tradable things handled by this mod. (Read: Humans are animals, but not for this purpose.)
                            Faction faction = FactionUtility.DefaultFactionFrom(pawnKind.defaultFactionType);
                            if (faction.AllyOrNeutralTo(Faction.OfPlayer)) {
                                if (pawnKind.RaceProps.hasGenders) {  // If has genders
                                    for (int g = 1; g < Enum.GetNames(typeof(Gender)).Length; ++g) {  // Get the amount of genders in the game (minus one because non-gender is handled seperately), in case there's a creature with more than 2 genders.
                                        Pawn pawn = PawnGenerator.GeneratePawn(pawnKind, faction: faction);
                                        pawn.gender = (Gender) g;
                                        Pawn spawnedPawn = (Pawn) GenSpawn.Spawn(pawn, loc, map, wipeMode: WipeMode.VanishOrMoveAside);  // Thing thing SHOULD just be pawn, but now on world. SHOULD be safe to use as (Pawn).
                                        // I think Lords are the thing controlling group AI, and faction-traveler-group herds, herdbehaviour, and who is part of a given group. However I think in our case putting spawnedPawn into the group of their faction if they are on the map, would not be useful, and infact might even me negative, due to us trying to tame them!
                                        // src: Verse.DebugToolsSpawning.SpawnPawn
                                        /*if (faction != null && faction != Faction.OfPlayer) {
                                            Lord lord = null;
                                            if (spawnedPawn.Map.mapPawns.SpawnedPawnsInFaction(faction).Any((Pawn p) => p != spawnedPawn)) {  // If there's another Pawn of the same faction on the map (Who isn't spawnedPawn)
                                                lord = ((Pawn) GenClosest.ClosestThing_Global(spawnedPawn.Position, spawnedPawn.Map.mapPawns.SpawnedPawnsInFaction(faction), 99999f, delegate (Thing p) {  // Search for that pawn
                                                    if (p != spawnedPawn) {  // (Double?) Check that that pawn is infact not spawnedPawn
                                                        return ((Pawn) p).GetLord() != null;  // Yell that there is a "Lord" (I think a Lord is like a group/herd AI?)
                                                    }
                                                    return false;  // No Lord found
                                                }, null)).GetLord();  // Grab the found Lord (if any)
                                            }
                                            if (lord == null) {  // If no usable Lord found, make a new one!
                                                LordJob_DefendPoint lordJob = new LordJob_DefendPoint(spawnedPawn.Position);
                                                lord = LordMaker.MakeNewLord(faction, lordJob, Find.CurrentMap, null);
                                            }
                                            lord.AddPawn(spawnedPawn);
                                        }*/
                                        ++totalSpawnedThings;
                                        spawnedThings.Add(pawn);
                                    }
                                } else {  // Non-gender
                                    Pawn pawn = PawnGenerator.GeneratePawn(pawnKind, faction: faction);
                                    // [POSSIBLE FUTURE BUG] I ASSUME that things without gender automatically gender themselves to the correct gender. If not, manually do that here.
                                    Pawn spawnedPawn = (Pawn) GenSpawn.Spawn(pawn, loc, map, wipeMode: WipeMode.VanishOrMoveAside);
                                    ++totalSpawnedThings;
                                    spawnedThings.Add(pawn);
                                }
                            } else {  // else Hostile [POSSIBLE FUTURE MISSING FEATURE] What about things hostile-by-faction, that should be tamed?
                                thingsNotSpawned.Add(pawnKind);
                            }
                        } else { // [POSSIBLE FUTURE MISSING FEATURE/BUG] What if there are humans that should be tamad, or worse, things that aren't human, but when tamed become colonists! D:
                            thingsNotSpawned.Add(pawnKind);
                        }
                    }
                    catch (Exception e) {
                        Log.Error("There was an error trying to spawn " + pawnKind.ToString() + "! (Continuing if possible)");
                        throw e;
                    }
                }
                String spawnedThings_String = "";
                foreach (Pawn pawn in spawnedThings) {
                    spawnedThings_String += "(" + pawn.kindDef.race.ToString() + ", " + pawn.gender.ToString() + "), ";
                }
                Log.Message("Spawned " + totalSpawnedThings.ToString() + " animals!\n" + spawnedThings_String.Substring(0, spawnedThings_String.Length - 2));

                String thingsNotSpawned_String = "";
                foreach (PawnKindDef pawn in thingsNotSpawned) {
                    thingsNotSpawned_String += pawn.ToString() + ", ";
                }
                Log.Message(thingsNotSpawned.Count().ToString() + " things NOT spawned!\n" + thingsNotSpawned_String.Substring(0, thingsNotSpawned_String.Length - 2));
            } else {
                Log.Error("You can't spawn a ship outside of a game/map!");
            }
        }

        [DebugAction("Spawning", "Spawn all items", actionType = DebugActionType.ToolMap, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void SpawnAllItems() {
            SpawnAllItems(Find.CurrentMap, UI.MouseCell());
        }

        public static void SpawnAllItems(Map map, IntVec3 loc, string filter = null) {
            bool useFilter = false;
            if (filter != "" && filter != null) {
                useFilter = true;
            }

            if (map != null) {
                int totalSpawnedThings = 0;
                List<ThingDef> listOfThingsToSpawn = new List<ThingDef>();

                foreach (ThingDef thingDef in DefDatabase<ThingDef>.AllDefs) {
                    if (thingDef.EverHaulable) {
                        if (DebugThingPlaceHelper.IsDebugSpawnable(thingDef)) {
                            if (useFilter) {
                                if (thingDef.defName.Contains(filter)) {
                                    listOfThingsToSpawn.Add(thingDef);
                                }
                            } else {
                                listOfThingsToSpawn.Add(thingDef);
                            }
                        }
                    }
                }

                string spawnedItems = "";
                foreach (ThingDef thingDef in listOfThingsToSpawn) {
                    DebugThingPlaceHelper.DebugSpawn(thingDef, loc);
                    spawnedItems += thingDef.label + ", ";
                    ++totalSpawnedThings;
                }

                Log.Message(totalSpawnedThings.ToString() + " Items spawned.\n" + spawnedItems.Substring(0, spawnedItems.Length - 2));
            } else {
                Log.Error("You can't spawn items outside of a game/map!");
            }
        }

        [DebugAction("Pawns", "Tame all animals", allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void TameAllAnimals() {
            TameAllAnimals(Find.CurrentMap, Find.CurrentMap.mapPawns.FreeColonists.FirstOrDefault());
        }

        public static void TameAllAnimals(Map map, Pawn tamer) {
            /*if (tamer.Faction == null) {  // taming to null Faction allowed, as a way to untame all.
                throw new ArgumentNullException("tamer.Faction");
            }*/
            if (map != null) {
                int thingsTamed = 0;
                Log.Message("Number of currently spawned Pawns on map: " + map.mapPawns.AllPawnsSpawnedCount.ToString());
                List<Pawn> allPawnsSpawned_old = map.mapPawns.AllPawnsSpawned.ListFullCopy();  // "System.InvalidOperationException: Collection was modified; enumeration operation may not execute." ... So we copy it and work with the copy! (This may work, or may go horribly wrong, idk...))
                List<Pawn> failedToTame = new List<Pawn>();
                foreach (Pawn pawn in allPawnsSpawned_old) {  // However tempting, let us not try to tame dead or unspawned things...
                    if (TameUtility.CanTame(pawn)) {  // If tameable
                        InteractionWorker_RecruitAttempt.DoRecruit(tamer, (Pawn) pawn, true);  // ! POSSIBLE FUTURE BUG !, If you have a colonist from another faction (like a hired mercenary, or royal aid), and they train something... Might that give the pawn to their faction, and thereby make it not tradable?
                        // 1.2 -> 1.3: InteractionWorker_RecruitAttempt.DoRecruit(Pawn recruiter, Pawn recruitee, float recruitChance, [bool useAudiovisualEffects = true]) -> InteractionWorker_RecruitAttempt.DoRecruit(Pawn recruiter, Pawn recruitee, [bool useAudiovisualEffects = true])
                        ++thingsTamed;
                    } else {
                        if (pawn.Faction != tamer.Faction) {  // Closest to "if (tame)" I can think of.
                            failedToTame.Add(pawn);
                        }
                    }
                }
                if (failedToTame.Count != 0) {
                    string failedToTame_String = "";
                    foreach (Pawn pawn in failedToTame) {
                        failedToTame_String += "(" + pawn.KindLabel + ", " + pawn.Name + ", (" + pawn.ToString() + ")), ";
                    }
                    Log.Error("Failed to tame " + failedToTame.Count.ToString() + " Pawns!\n" + failedToTame_String.Substring(0, failedToTame_String.Length - 2));
                }
                Log.Message("Number of things tamed: " + thingsTamed.ToString());
            } else {
                Log.Error("You can't tame everything on a map, when you have no map!");
            }
        }
    }
}

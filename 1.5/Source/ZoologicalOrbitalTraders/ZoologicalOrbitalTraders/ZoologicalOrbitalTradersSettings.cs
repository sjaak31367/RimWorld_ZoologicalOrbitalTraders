﻿using System;
using UnityEngine;
using Verse;
using RimWorld;
using LudeonTK;

namespace ZoologicalOrbitalTraders {
    public class ZoologicalOrbitalTradersSettings : ModSettings {
        public float zoologicalOrbitalTrader_commonality = Single.NaN;
        public bool showDebugSettings = false;

        public const int commonalityPresets = 7;
        private readonly float[] arrayCommonalityLetsTrade = new float[commonalityPresets] { 0f, 2f, 5f, 10f, 15f, 20f, 40f };
        public float[] arrayCommonalityVanilla = new float[commonalityPresets] { 0f, 0.33f, 0.66f, 1f, 1.5f, 2f, 3f };

        private string editBufferString = "";
        private float editBufferFloat;
        private bool isFirstFrame = true;  // Used for doing stuff only during the first rendered frame.
        private bool considerLetsTrade = ModLister.HasActiveModWithName("Let's Trade!") || ModLister.HasActiveModWithName("Let's Trade! [Continued]");
        private readonly bool isLetsTradeInstalled = ModLister.HasActiveModWithName("Let's Trade!") || ModLister.HasActiveModWithName("Let's Trade! [Continued]");

        public void DoWindowContents (Rect canvas) {
            var options = new Listing_Standard();
            options.Begin(canvas);
            if (isFirstFrame) {
                UpdateDisplayValues();
                isFirstFrame = false;
            }
            //options.IntAdjuster(ref temp, 1);  // -1 and +1 button
            //options.IntEntry(ref temp, ref editBuffer);  // -10, -1, textbox, +1, +10(make sure editBuffer is outside of DoWindowContents!)
            //options.IntRange(ref ir, 0, 100);  // slider with two half-dots, for lower- and upper- bound (example: A~B)
            //options.IntSetter(ref temp, 32, "text?");  // button, press to set variable to specific value.
            //options.TextFieldNumeric<int>(ref temp, ref buffer);  // Textinputbox (which only allows numeric), no buttons, immidiate assign.

            /*if (ModLister.HasActiveModWithName("Let's Trade!")) {  // Check if X mod exists, and if so, show a Label
                options.Label("LT exists!");
            }
            foreach (var item in LoadedModManager.RunningMods.ToList()) {  // Print all running mods to screen
                options.Label(item.Name);
            }*/
            options.Label((isLetsTradeInstalled ? "Let's Trade!" : "Vanilla") + " trading system detected!");
            options.Label("Using " + (considerLetsTrade ? "Let's Trade!" : "vanilla") + " trader-spawning chances.", tooltip: "Let's Trade! " + (isLetsTradeInstalled ? "<b>is</b>" : "is <b>not</b>") + " installed!\n(And that's okay.)\n(However, using the wrong mode will cause either extemely few, or extremely many Zoological Orbital Traders to show up.)");

            Rect buttonSwitchMultiplier = new Rect(canvas.width - canvas.width / 3, 18, canvas.width / 3, 28);  // If the gamefont-size changes, it'll slightly misalign... Maybe... Or things  might go apesh*t, who knows? (Edge-case, just hope it doesn't happen...)
            if (Widgets.ButtonText(buttonSwitchMultiplier, "Change spawn-chance calculator")) {  // When changing calculator type, also change the current value.
                considerLetsTrade = !considerLetsTrade;
                int arrayIndex = Array.IndexOf(considerLetsTrade ? arrayCommonalityVanilla : arrayCommonalityLetsTrade, zoologicalOrbitalTrader_commonality);
                if (0 < arrayIndex && arrayIndex < commonalityPresets) {
                    zoologicalOrbitalTrader_commonality = (considerLetsTrade ? arrayCommonalityLetsTrade : arrayCommonalityVanilla) [arrayIndex];
                } else {
                    zoologicalOrbitalTrader_commonality = zoologicalOrbitalTrader_commonality * (considerLetsTrade ? 10f : 0.1f);
                }
                UpdateDisplayValues();
            }

            //options.Gap();
            options.GapLine();
            options.Label("Commonality: " + zoologicalOrbitalTrader_commonality.ToString(), tooltip: "How common Zoological Orbital Traders are.\nVanilla traders have values of 0~3." + (isLetsTradeInstalled ? "\nLet's Trade! Has values of 0~40." : ""));
            options.Label("(After changing this value please restart RimWorld for the new settings to take effect.)");  // TODO: Maybe try and get another way to edit the value, preferably one which doesn require a game restart to apply.

            float[] buttonAttributes = new float[4] { canvas.width * 0.675f, canvas.height * 0.125f, canvas.width / 4, 28 };

            Rect buttonNever = new Rect(buttonAttributes[0], buttonAttributes[1] + buttonAttributes[3] * 0, buttonAttributes[2], buttonAttributes[3]);
            if (Widgets.ButtonText(buttonNever, "Never")) {
                zoologicalOrbitalTrader_commonality = considerLetsTrade ? arrayCommonalityLetsTrade[0] : arrayCommonalityVanilla[0];  // Never
                UpdateDisplayValues();
            }
            Rect buttonRare = new Rect(buttonAttributes[0], buttonAttributes[1] + buttonAttributes[3] * 1, buttonAttributes[2], buttonAttributes[3]);
            if (Widgets.ButtonText(buttonRare, "Rare")) {
                zoologicalOrbitalTrader_commonality = considerLetsTrade ? arrayCommonalityLetsTrade[1] : arrayCommonalityVanilla[1];  // Rare
                UpdateDisplayValues();
            }
            Rect buttonUncommon = new Rect(buttonAttributes[0], buttonAttributes[1] + buttonAttributes[3] * 2, buttonAttributes[2], buttonAttributes[3]);
            if (Widgets.ButtonText(buttonUncommon, "Uncommon")) {
                zoologicalOrbitalTrader_commonality = considerLetsTrade ? arrayCommonalityLetsTrade[2] : arrayCommonalityVanilla[2];  // Uncommon
                UpdateDisplayValues();
            }
            Rect buttonAverage = new Rect(buttonAttributes[0], buttonAttributes[1] + buttonAttributes[3] * 3, buttonAttributes[2], buttonAttributes[3]);
            if (Widgets.ButtonText(buttonAverage, "Average (default)")) {
                zoologicalOrbitalTrader_commonality = considerLetsTrade ? arrayCommonalityLetsTrade[3] : arrayCommonalityVanilla[3];  // Average
                UpdateDisplayValues();
            }
            Rect buttonCommon = new Rect(buttonAttributes[0], buttonAttributes[1] + buttonAttributes[3] * 4, buttonAttributes[2], buttonAttributes[3]);
            if (Widgets.ButtonText(buttonCommon, "Common")) {
                zoologicalOrbitalTrader_commonality = considerLetsTrade ? arrayCommonalityLetsTrade[4] : arrayCommonalityVanilla[4];  // Common
                UpdateDisplayValues();
            }
            Rect buttonVeryCommon = new Rect(buttonAttributes[0], buttonAttributes[1] + buttonAttributes[3] * 5, buttonAttributes[2], buttonAttributes[3]);
            if (Widgets.ButtonText(buttonVeryCommon, "Very Common")) {
                zoologicalOrbitalTrader_commonality = considerLetsTrade ? arrayCommonalityLetsTrade[5] : arrayCommonalityVanilla[5];  // Very Common
                UpdateDisplayValues();
            }
            Rect buttonAbundant = new Rect(buttonAttributes[0], buttonAttributes[1] + buttonAttributes[3] * 6, buttonAttributes[2], buttonAttributes[3]);
            if (Widgets.ButtonText(buttonAbundant, "Abundant")) {
                zoologicalOrbitalTrader_commonality = (considerLetsTrade ? arrayCommonalityLetsTrade[6] : arrayCommonalityVanilla[6]);  // Abundant
                UpdateDisplayValues();
            }

            // Small inputbox with button to manually set the value.
            Rect manualInputBox = new Rect(buttonAttributes[0], buttonAttributes[1] + buttonAttributes[3] * 7, buttonAttributes[2]-buttonAttributes[2]/5, buttonAttributes[3]);
            Widgets.TextFieldNumeric<float>(manualInputBox, ref editBufferFloat, ref editBufferString);
            Rect manualInputButton = new Rect(buttonAttributes[0]+buttonAttributes[2]*4/5, buttonAttributes[1] + buttonAttributes[3] * 7, buttonAttributes[2] / 5, buttonAttributes[3]);
            if (Widgets.ButtonText(manualInputButton, "Set")) {
                zoologicalOrbitalTrader_commonality = editBufferFloat;
            }

            float[] debugOptionsRectAttributes = new float[4] { 0f, canvas.height * 0.55f, canvas.width * 0.4f, 28 };
            Widgets.Label(new Rect(debugOptionsRectAttributes[0], debugOptionsRectAttributes[1], canvas.width * 0.165f, debugOptionsRectAttributes[3]), "Show debug controls: ");
            Widgets.Checkbox(canvas.width * 0.165f, debugOptionsRectAttributes[1], ref showDebugSettings);
            if (showDebugSettings) {  // (was Prefs.DevMode)
                //options.Gap(gapHeight: buttonAttributes[3] * commonalityPresets);
                //options.GapLine();
                Rect buttonSpawnTrader = new Rect(debugOptionsRectAttributes[0], debugOptionsRectAttributes[1] + debugOptionsRectAttributes[3] * 1, debugOptionsRectAttributes[2], debugOptionsRectAttributes[3]);
                if (Widgets.ButtonText(buttonSpawnTrader, "Force spawn a Zoological Orbital Trader")) {
                    if (Find.CurrentMap != null) {
                        TraderKindDef traderKindDef = DefDatabase<TraderKindDef>.GetNamed("Orbital_ZoologicalTrader");
                        ZoologicalObritalTradersTools.SpawnSpecificOrbitalTrader(traderKindDef, "The Buggers", Find.CurrentMap, "Zoological trader", "A requested tradeship has arrived.\n\nThey are known to aid with debugging. They are a zoological trader.\n\nThey are not affiliated with any faction.");
                    } else {
                        Log.Error("You can't spawn a ship outside of a game/map!");
                    }
                }
                Rect buttonSpawnAllAnimals = new Rect(debugOptionsRectAttributes[0], debugOptionsRectAttributes[1] + debugOptionsRectAttributes[3] * 2, debugOptionsRectAttributes[2], debugOptionsRectAttributes[3]);
                if (Widgets.ButtonText(buttonSpawnAllAnimals, "Tool: Spawn all animals")) {
                    if (Find.CurrentMap != null) {
                        DebugTools.curTool = new DebugTool("Spawn all animals", ZoologicalObritalTradersTools.SpawnAllAnimals);
                    } else {
                        Log.Error("This debug tool cannot be equiped outside of a game/map!");
                    }
                }
                Rect buttonTameEverything = new Rect(debugOptionsRectAttributes[0], debugOptionsRectAttributes[1] + debugOptionsRectAttributes[3] * 3, debugOptionsRectAttributes[2], debugOptionsRectAttributes[3]);
                if (Widgets.ButtonText(buttonTameEverything, "Tame everything on map")) {
                    if (Find.CurrentMap != null) {
                        ZoologicalObritalTradersTools.TameAllAnimals();
                    } else {
                        Log.Error("You can't tame everything on a map, when you are not on a map!");
                    }
                }
                Rect buttonSpawnAllItems = new Rect(debugOptionsRectAttributes[0], debugOptionsRectAttributes[1] + debugOptionsRectAttributes[3] * 4, debugOptionsRectAttributes[2], debugOptionsRectAttributes[3]);
                if (Widgets.ButtonText(buttonSpawnAllItems, "Tool: Spawn all items")) {
                    if (Find.CurrentMap != null) {
                        DebugTools.curTool = new DebugTool("Spawn all items", ZoologicalObritalTradersTools.SpawnAllItems);
                    } else {
                        Log.Error("This debug tool cannot be equiped outside of a game/map!");
                    }
                }
            }
            options.End();
        }

        public override void ExposeData() {
            base.ExposeData();  // ? TODO ? maybe getting rid of this line would actually make the defaults generate properly, maybe not? IDK, I've got a workaround now.
            Scribe_Values.Look(ref zoologicalOrbitalTrader_commonality, "commonality", (isLetsTradeInstalled ? 10 : 1));
            Scribe_Values.Look(ref showDebugSettings, "show_debug_settings", false);
        }

        public void UpdateDisplayValues() {
            editBufferString = zoologicalOrbitalTrader_commonality.ToString();  // If this was string to float, this would be very unsanitary.
            editBufferFloat = zoologicalOrbitalTrader_commonality;  // Not display, but otherwise hitting "Set" may or may not set the commonality to a value that's not currently displayed.
        }
    }
}
